/* Copyright (C) 2019 Nathan Taylor - All Rights Reserved
 *
 * This file is part of Drakurynth.
 *
 * Drakurynth is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Drakurynth is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Drakurynth.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <string>
// imply std::
using namespace std;

void print_menu()
{
    // copyright notice
    cout << "---\n"
            "Drakurynth  Copyright (C) 2019  Nathan Taylor\n"
            "This program comes with ABSOLUTELY NO WARRANTY\n"
            "This is free software, and you are welcome to redistribute it"
            " under certain conditions.\n"
            "---\n\n";

    // menu options
    cout << "1 - New Game\n"
            "2 - Load Game\n"
            "3 - Options\n"
            "4 - Quit\n\n";
}

void enter_to_continue()
{
    cout << "Press Enter to continue\n";
    // this loops through any amount of characters entered until it sees '\n'
    while (cin.get() != '\n');
    cout << string(8, '\n');
}

void set_user_name()
{
    // input name
    string usrName;
    cout << "Enter your name: ";
    getline(cin, usrName);
    cout << "You win, but your name is still " << usrName << ".\n";
    enter_to_continue();
}

void menu_choices()
{
    while (true)
    {
        print_menu();
        // input menu choice
        string usrInput;
        cout << "Enter a choice: ";
        getline(cin, usrInput);
        // process choice
        if (usrInput == "1")
        {
            set_user_name();
        }
        else if (usrInput == "2")
        {
            cout << "There are no saves.\n";
            enter_to_continue();
        }
        else if (usrInput == "3")
        {
            cout << "There are no options.\n";
            enter_to_continue();
        }
        else if (usrInput == "4")
        {
            cout << "Bye.\n";
            return;
        }
        else
            cout << "Invalid choice.\n";
    }
}

int main()
{
    menu_choices();
    // terminate the program
    return 0;
}
